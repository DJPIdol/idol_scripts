#!/bin/bash
sudo cp $1 /lib/systemd/system/$1.service
sudo chmod 755 /lib/systemd/system/$1.service
sudo chown root /lib/systemd/system/$1.service
sudo chgrp root /lib/systemd/system/$1.service
sudo systemctl enable $1
